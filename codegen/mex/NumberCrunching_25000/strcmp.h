/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * strcmp.h
 *
 * Code generation for function 'strcmp'
 *
 */

#ifndef __STRCMP_H__
#define __STRCMP_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_25000_types.h"

/* Function Declarations */
extern boolean_T b_strcmp(const char_T a[4]);
extern boolean_T c_strcmp(const char_T a[4]);

#endif

/* End of code generation (strcmp.h) */
