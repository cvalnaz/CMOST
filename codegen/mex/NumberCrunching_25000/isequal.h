/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * isequal.h
 *
 * Code generation for function 'isequal'
 *
 */

#ifndef __ISEQUAL_H__
#define __ISEQUAL_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_25000_types.h"

/* Function Declarations */
extern boolean_T isequal(real_T varargin_1, real_T varargin_2);

#endif

/* End of code generation (isequal.h) */
