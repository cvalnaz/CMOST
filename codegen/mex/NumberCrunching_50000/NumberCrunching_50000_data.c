/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * NumberCrunching_50000_data.c
 *
 * Code generation for function 'NumberCrunching_50000_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "NumberCrunching_50000.h"
#include "NumberCrunching_50000_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true, false, 131419U, NULL,
  "NumberCrunching_50000", NULL, false, { 2045744189U, 2170104910U, 2743257031U,
    4284093946U }, NULL };

emlrtRSInfo cb_emlrtRSI = { 773, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo mc_emlrtRSI = { 1331, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo nc_emlrtRSI = { 1338, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo oc_emlrtRSI = { 1385, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo pc_emlrtRSI = { 1396, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo qc_emlrtRSI = { 1471, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo rc_emlrtRSI = { 1475, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo tc_emlrtRSI = { 1483, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo uc_emlrtRSI = { 1487, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo vc_emlrtRSI = { 1491, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo wc_emlrtRSI = { 1495, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo xc_emlrtRSI = { 1528, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo yc_emlrtRSI = { 1538, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo ad_emlrtRSI = { 1557, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo bd_emlrtRSI = { 1584, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo cd_emlrtRSI = { 1606, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo dd_emlrtRSI = { 1623, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo ed_emlrtRSI = { 1639, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

emlrtRSInfo fd_emlrtRSI = { 1643, "NumberCrunching_50000",
  "U:\\CMOST\\CMOST_01_August_2016\\NumberCrunching_50000.m" };

/* End of code generation (NumberCrunching_50000_data.c) */
